# Preferred Networks インターン選考2019 コーディング課題 機械学習・数理分野

## 実験に使用した計算機環境・ライブラリ等
アルゴリズム実装にRustを、グラフの描画にPythonを使用しました。

- Rust 1.34.1 stable
  - ライブラリとして[ndarray](https://github.com/rust-ndarray/ndarray) 0.12.1 を使用
- Python 3.7.3
  - ライブラリとして[matplotlib](https://matplotlib.org) 3.0.2 を使用
- OS
  - Arch Linux 5.0.9
  
### Rustコンパイラのバージョンについて

確認はしていませんが1.31.0以降であれば動くと思います。

## 実行手順

課題3,4におけるコマンドはすべて`datasets`が`Cargo.toml`と同じディレク
トリにあることを仮定しています。

別のディレクトリにある場合は `--datadir=/home/user/Documents/datasets`のように
してデータセットの場所を指定して下さい。

### 課題1

``` bash
cargo test test_hg
```

コードは `src/graph.rs` および `src/mlp.rs` にあります。

コードの共通化のため、aggregateに線形変換を用いる場合は、バイアスなし・
レイヤー数1のMLPとして実装されています。

### 課題2

``` bash
cargo test test_grad_descent_linear
```

コードは `src/classifier.rs` および `src/mlp.rs` にあります。

### 課題3
``` bash
cargo run --release -- --epochs=40 --alpha=0.0005
```
ミニバッチサンプリングは `src/train.rs` に、Momentum SGD は
`src/optimizer.rs`にあります。

### 課題4
2層のMLPをaggregationに用いるものを実装しました。また、Adamも実装しま
した。
コードは`src/mlp.rs`および`src/optimizer.rs`にあります。

Momentum SGDを使用する場合
``` bash
cargo run --release -- --bias=true --layers=2 --epochs=80 --alpha=0.0005
```

Adamを使用する場合
``` bash
cargo run --release -- --bias=true --layers=2 --epochs=80 --alpha=0.0005 --optim=adam
```

線形変換でAdamを使用する場合
``` bash
cargo run --release -- --epochs=80 --alpha=0.0005 --optim=adam
```

prediction.txtを生成するのに使用したコマンド
``` bash
cargo run --release -- --bias=true --layers=2 --epochs=80 --alpha=0.0005 --prediction=true
```

### 実験結果のプロット
``` bash
python plot.py result.json
```
jsonファイルを複数指定すると複数同時にプロットできます。



