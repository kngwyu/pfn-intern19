use crate::graph::Graph;
use crate::mlp::Mlp;
use crate::prelude::*;
use crate::random;

#[inline(always)]
fn sigmoid(logit: Scalar) -> Scalar {
    1.0 / (1.0 + (-logit).exp())
}

/// Calcurate the binary cross entropy loss from the logit and the label.
#[inline(always)]
fn bce_from_logit(logit: Scalar, label: Scalar) -> Scalar {
    label * (1.0 + (-logit).exp()).loge() + (1.0 - label) * (1.0 + logit.exp()).loge()
}

fn calc_bce_loss(
    mlp: &Mlp,
    a: VectorView,
    b: Scalar,
    graph: &Graph,
    label: Scalar,
    t: usize,
) -> Scalar {
    let hg = graph.calc_hg(t, mlp).0;
    let logit = a.dot(&hg) + b;
    let res = bce_from_logit(logit, label);
    res
}

/// A set of parameters of a graph NN, which can be used as a classifier.
#[derive(Clone, Debug)]
pub struct Classifier {
    mlp: Mlp,
    a: Vector,
    b: Scalar,
}

impl Classifier {
    /// Returns a classifier of which all parameters are filled with 0.
    pub fn zeros_like(&self) -> Self {
        Classifier {
            mlp: self.mlp.zeros_like(),
            a: Vector::zeros(self.a.dim()),
            b: 0.0,
        }
    }

    /// Returns a classifier which has an MLP aggregator with `num_layers` layers
    /// and biases if `has_bias` is specified.
    ///
    /// All weights are sampled from N(0, 0.4) and biases are 0.
    pub fn new(w_dim: usize, num_layers: usize, has_bias: bool) -> Self {
        Classifier {
            mlp: Mlp::new(w_dim, num_layers, has_bias),
            a: random::randn(w_dim, 0.0, 0.4),
            b: 0.0,
        }
    }

    /// Applies `f` to all parameters
    pub fn for_each(&mut self, f: impl Fn(&mut Scalar)) {
        self.mlp.for_each(&f);
        self.a.iter_mut().for_each(&f);
        f(&mut self.b);
    }

    /// Applies `f` to all parameters with an other parameter set `other`.
    pub fn zip2_for_each(&mut self, other: &Classifier, updater: impl Fn(&mut Scalar, &Scalar)) {
        self.mlp.zip2_for_each(&other.mlp, &updater);
        Zip::from(&mut self.a).and(&other.a).apply(&updater);
        updater(&mut self.b, &other.b);
    }

    /// Applies `f` to all parameters with two other parameter sets.
    pub fn zip3_for_each(
        &mut self,
        other1: &Classifier,
        other2: &Classifier,
        updater: impl Fn(&mut Scalar, &Scalar, &Scalar),
    ) {
        self.mlp.zip3_for_each(&other1.mlp, &other2.mlp, &updater);
        Zip::from(&mut self.a)
            .and(&other1.a)
            .and(&other2.a)
            .apply(&updater);
        updater(&mut self.b, &other1.b, &other2.b);
    }

    /// Applies the gradient descent method
    pub fn grad_descent(&mut self, grad: &Classifier, alpha: Scalar) {
        self.zip2_for_each(grad, |w, g| *w -= alpha * g)
    }

    /// Calculates the numerical gradient using bce loss
    pub fn calc_grad(
        &self,
        graph: &Graph,
        label: Scalar,
        t: usize,
        eps: Scalar,
    ) -> (Classifier, Scalar) {
        let loss = calc_bce_loss(&self.mlp, self.a.view(), self.b, graph, label, t);
        let dl_dw = self.mlp.calc_grad(eps, loss, |mlp| {
            calc_bce_loss(mlp, self.a.view(), self.b, graph, label, t)
        });
        let dl_da = self.calc_dl_da(eps, loss, |a| {
            calc_bce_loss(&self.mlp, a, self.b, graph, label, t)
        });
        let dl_db = self.calc_dl_db(eps, loss, |b| {
            calc_bce_loss(&self.mlp, self.a.view(), b, graph, label, t)
        });
        (
            Classifier {
                mlp: dl_dw,
                a: dl_da,
                b: dl_db,
            },
            loss,
        )
    }

    /// Returns a lebel prediction for a graph.
    pub fn predict(&self, graph: &Graph, t: usize) -> Scalar {
        let hg = graph.calc_hg(t, &self.mlp).0;
        let logit = self.a.dot(&hg) + self.b;
        sigmoid(logit)
    }

    fn calc_dl_da(
        &self,
        eps: Scalar,
        loss: Scalar,
        calc_loss: impl Fn(VectorView) -> Scalar,
    ) -> Vector {
        let mut a = self.a.clone();
        let mut grad = Vector::zeros(a.dim());
        for x in 0..a.dim() {
            a[x] += eps;
            let neighbor_loss = calc_loss(a.view());
            grad[x] = (neighbor_loss - loss) / eps;
            a[x] = self.a[x]
        }
        grad
    }

    fn calc_dl_db(
        &self,
        eps: Scalar,
        loss: Scalar,
        calc_loss: impl Fn(Scalar) -> Scalar,
    ) -> Scalar {
        let neighbor_loss = calc_loss(self.b + eps);
        (neighbor_loss - loss) / eps
    }
}

#[cfg(test)]
mod test {
    use super::*;
    const D: usize = 4;

    fn common(mut classifier: Classifier) {
        let adj = array![
            [0., 0., 0., 0., 0., 1., 0., 0., 0., 0.],
            [0., 0., 0., 1., 0., 0., 1., 0., 0., 1.],
            [0., 0., 0., 1., 0., 0., 0., 0., 1., 1.],
            [0., 1., 1., 0., 1., 0., 1., 1., 0., 0.],
            [0., 0., 0., 1., 0., 1., 1., 0., 1., 0.],
            [1., 0., 0., 0., 1., 0., 1., 0., 0., 1.],
            [0., 1., 0., 1., 1., 1., 0., 1., 1., 0.],
            [0., 0., 0., 1., 0., 0., 1., 0., 0., 0.],
            [0., 0., 1., 0., 1., 0., 1., 0., 0., 1.],
            [0., 1., 1., 0., 0., 1., 0., 0., 1., 0.]
        ];
        let graph = Graph::from_x0gen(adj, crate::graph::x0gen_const(D, 1.0));
        assert_eq!(classifier.a.dim(), 4);
        let mut loss_before = 1e10;
        for _ in 0..10 {
            let (grad, loss) = classifier.calc_grad(&graph, 1.0, 2, 0.001);
            classifier.grad_descent(&grad, 0.0001);
            assert!(loss_before > loss);
            loss_before = loss;
        }
    }

    #[test]
    fn test_grad_descent_linear() {
        common(Classifier::new(D, 1, false))
    }

    #[test]
    fn test_grad_descent_mlp() {
        common(Classifier::new(D, 2, true))
    }
}
