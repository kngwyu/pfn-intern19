use crate::prelude::*;
use crate::random;

#[inline(always)]
fn inplace_relu(mut m: MatrixViewMut) {
    m.iter_mut().for_each(|x| {
        if *x < 0.0 {
            *x = 0.0
        }
    })
}

/// Multilayer perceptron with which all layers has the same shape DxD.
#[derive(Clone, Debug)]
pub struct Mlp {
    /// (D*layers)xD matrix
    weight: Matrix,
    /// layersxD matrix
    bias: Matrix,
    layers: usize,
}

impl Mlp {
    pub fn new(w_dim: usize, layers: usize, has_bias: bool) -> Self {
        let weight = random::randn((w_dim * layers, w_dim), 0.0, 0.4);
        let bias = if has_bias {
            random::randn((layers, w_dim), 0.0, 0.4)
        } else {
            Matrix::zeros((0, 0))
        };
        Mlp {
            weight,
            bias,
            layers,
        }
    }
    pub fn zeros_like(&self) -> Self {
        Mlp {
            weight: Matrix::zeros(self.weight.dim()),
            bias: Matrix::zeros(self.bias.dim()),
            layers: self.layers,
        }
    }

    /// Applies the MLP operation to tha matrix `input`
    pub fn forward(&self, input: MatrixView) -> Matrix {
        let mut out = input.to_owned();
        let block = self.weight.dim().1;
        for l in 0..self.layers {
            let w = self.weight.slice(s![block * l..block * (l + 1), ..]);
            out = w.dot(&input).t().to_owned();
            if self.bias.dim().0 > 0 {
                out = out + self.bias.slice(s![l..l + 1, ..]);
            }
            inplace_relu(out.view_mut());
        }
        out
    }

    pub fn for_each(&mut self, f: impl Fn(&mut Scalar)) {
        self.weight.iter_mut().for_each(&f);
        self.bias.iter_mut().for_each(&f);
    }

    pub fn zip2_for_each(&mut self, other: &Self, updater: impl Fn(&mut Scalar, &Scalar)) {
        Zip::from(&mut self.weight)
            .and(&other.weight)
            .apply(&updater);
        Zip::from(&mut self.bias).and(&other.bias).apply(updater);
    }

    pub fn zip3_for_each(
        &mut self,
        other1: &Self,
        other2: &Self,
        updater: impl Fn(&mut Scalar, &Scalar, &Scalar),
    ) {
        Zip::from(&mut self.weight)
            .and(&other1.weight)
            .and(&other2.weight)
            .apply(&updater);
        Zip::from(&mut self.bias)
            .and(&other1.bias)
            .and(&other2.bias)
            .apply(&updater);
    }

    /// Calculates the numerical gradient using `loss` and `calc_loss`
    pub fn calc_grad(&self, eps: Scalar, loss: Scalar, calc_loss: impl Fn(&Mlp) -> Scalar) -> Mlp {
        let mut other = self.clone();
        let mut grad = self.zeros_like();
        macro_rules! calc_grad {
            ($param: expr, $grad: expr, $orig: expr) => {
                let shape = $param.dim();
                for y in 0..shape.0 {
                    for x in 0..shape.1 {
                        $param[[y, x]] += eps;
                        let neigbor_loss = calc_loss(&other);
                        $grad[[y, x]] = (neigbor_loss - loss) / eps;
                        $param[[y, x]] = $orig[[y, x]];
                    }
                }
            };
        }
        calc_grad!(other.weight, grad.weight, self.weight);
        calc_grad!(other.bias, grad.bias, self.bias);
        grad
    }

    #[cfg(test)]
    pub fn linear(w: Matrix) -> Self {
        Mlp {
            weight: w,
            bias: Matrix::zeros((0, 0)),
            layers: 1,
        }
    }
}
