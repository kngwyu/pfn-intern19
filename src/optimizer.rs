use crate::classifier::Classifier;
use crate::prelude::*;

pub trait Optimizer {
    fn update(&mut self, _grad: &Classifier, _params: &mut Classifier);
    fn reset(&mut self) {}
}

#[derive(Clone, Debug)]
pub struct Sgd {
    alpha: Scalar,
}

impl Sgd {
    pub fn new(alpha: Scalar) -> Self {
        Sgd { alpha }
    }
}

impl Optimizer for Sgd {
    fn update(&mut self, grad: &Classifier, params: &mut Classifier) {
        params.grad_descent(grad, self.alpha);
    }
}

#[derive(Clone, Debug)]
pub struct MomentumSgd {
    moment: Classifier,
    alpha: Scalar,
    eta: Scalar,
}

impl MomentumSgd {
    pub fn new(classifier: &Classifier, alpha: Scalar, eta: Scalar) -> Self {
        MomentumSgd {
            moment: classifier.zeros_like(),
            alpha,
            eta,
        }
    }
}

impl Optimizer for MomentumSgd {
    fn update(&mut self, grad: &Classifier, params: &mut Classifier) {
        let (alpha, eta) = (self.alpha, self.eta);
        self.moment
            .zip2_for_each(grad, |m, grad| *m = -alpha * grad + eta * *m);
        params.zip2_for_each(&self.moment, |param, m| *param += *m);
    }
    fn reset(&mut self) {
        self.moment.for_each(|m| *m = 0.0)
    }
}

#[derive(Clone, Debug)]
pub struct Adam {
    m: Classifier,
    v: Classifier,
    alpha: Scalar,
    beta1: Scalar,
    beta2: Scalar,
    epsilon: Scalar,
    t: i32,
}

impl Adam {
    pub fn new(
        classifier: &Classifier,
        alpha: Scalar,
        beta1: Scalar,
        beta2: Scalar,
        epsilon: Scalar,
    ) -> Adam {
        Adam {
            m: classifier.zeros_like(),
            v: classifier.zeros_like(),
            alpha,
            beta1,
            beta2,
            epsilon,
            t: 0,
        }
    }
}

impl Optimizer for Adam {
    fn update(&mut self, grad: &Classifier, params: &mut Classifier) {
        self.t += 1;
        let (alpha, beta1, beta2, eps) = (self.alpha, self.beta1, self.beta2, self.epsilon);
        self.m
            .zip2_for_each(grad, |m, g| *m = beta1 * *m + (1.0 - beta1) * g);
        self.v
            .zip2_for_each(grad, |v, g| *v = beta2 * *v + (1.0 - beta2) * g * g);
        let (beta1_t, beta2_t) = (beta1.powi(self.t), beta2.powi(self.t));
        params.zip3_for_each(&self.m, &self.v, |p, m, v| {
            let m_hat = m / (1.0 - beta1_t);
            let v_hat = v / (1.0 - beta2_t);
            *p -= alpha * m_hat / (v_hat.sqrt() + eps);
        });
    }
    fn reset(&mut self) {
        self.t = 0;
        self.m.for_each(|m| *m = 0.0);
        self.v.for_each(|v| *v = 0.0);
    }
}
