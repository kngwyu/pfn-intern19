mod classifier;
mod graph;
mod inference;
mod loader;
mod mlp;
mod optimizer;
mod prelude;
mod random;
mod train;

use crate::classifier::Classifier;
use crate::graph::*;
use crate::loader::*;
use crate::optimizer::{Adam, MomentumSgd, Optimizer, Sgd};
use crate::prelude::{Matrix, Scalar};
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, prelude::*};
use std::path::Path;

fn main() -> io::Result<()> {
    // get CLI arguments
    let args = ArgParse::new(std::env::args());
    let aggregate_t = args.get("t").unwrap_or(2);
    let d = args.get("d").unwrap_or(8);
    let minibatch_size = args.get("minibatch").unwrap_or(16);
    let num_epochs = args.get("epochs").unwrap_or(100);
    let epsilon = args.get("eps").unwrap_or(0.001);
    let num_layers = args.get("layers").unwrap_or(1);
    let has_bias = args.get("bias").unwrap_or(false);
    let dataset_dir = args.get("datadir").unwrap_or_else(default_data_dir);
    let result_file = args.get("result").unwrap_or_else(|| "result".to_string());
    let get_prediction = args.get("prediction").unwrap_or(false);
    let x0gen = get_x0gen(&args, d);

    let mut train_data = load_train_data(&dataset_dir, &*x0gen)?;
    let test_data = train_data.split_off(train_data.len() / 2);
    let mut classifier = Classifier::new(d, num_layers, has_bias);
    let mut optim = get_optimizer(&args, &classifier);
    let mut result = ExpResult::default();
    // Train the classifier `num_epochs` times
    for i in 0..num_epochs {
        let loss = train::train(
            &train_data,
            minibatch_size,
            aggregate_t,
            epsilon,
            &mut classifier,
            &mut *optim,
        );
        optim.reset();
        let train_ac = inference::test_classifier(&train_data, &classifier, aggregate_t);
        let test_ac = inference::test_classifier(&test_data, &classifier, aggregate_t);
        println!(
            "epoch: {}, loss: {}, train_accuracy: {}, test_accuracy: {}",
            i + 1,
            loss,
            train_ac,
            test_ac
        );
        result.push(loss, train_ac, test_ac);
    }
    result.write_to_file(format!("{}.json", result_file))?;
    if get_prediction {
        let data = load_test_data(dataset_dir, &*x0gen)?;
        let mut file = File::create("predicition.txt")?;
        for result in inference::inference(&data, &classifier, aggregate_t) {
            writeln!(file, "{}", result)?;
        }
    }
    Ok(())
}

/// Select the optimizer from command line arguments
fn get_optimizer(args: &ArgParse, classifier: &Classifier) -> Box<dyn Optimizer> {
    const MOMENTUM_SGD: &'static str = "momentum-sgd";
    let name = args
        .get("optim")
        .unwrap_or_else(|| MOMENTUM_SGD.to_string());
    let alpha = args.get("alpha").unwrap_or(0.0001);
    match &*name {
        "sgd" => Box::new(Sgd::new(alpha)),
        MOMENTUM_SGD => {
            let eta = args.get("eta").unwrap_or(0.9);
            Box::new(MomentumSgd::new(classifier, alpha, eta))
        }
        "adam" => {
            let beta1 = args.get("beta1").unwrap_or(0.9);
            let beta2 = args.get("beta2").unwrap_or(0.999);
            let eps = args.get("adam-eps").unwrap_or(1e-8);
            Box::new(Adam::new(classifier, alpha, beta1, beta2, eps))
        }
        _ => panic!("Invalid Optimizer: {}", &name),
    }
}

/// Select the initilizer for node features from command line arguments
fn get_x0gen(args: &ArgParse, d: usize) -> Box<dyn Fn(usize) -> Matrix> {
    let x0gen = args.get("x0gen").unwrap_or_else(|| "onehot".to_string());
    let initial_value = args.get("init").unwrap_or(1.0);
    if x0gen == "onehot" {
        Box::new(x0gen_onehot(d, initial_value))
    } else {
        Box::new(x0gen_const(d, initial_value))
    }
}

#[derive(Default)]
struct ExpResult {
    bce_losses: Vec<Scalar>,
    train_accuracies: Vec<Scalar>,
    test_accuracies: Vec<Scalar>,
}

impl ExpResult {
    fn push(&mut self, loss: Scalar, train_ac: Scalar, test_ac: Scalar) {
        self.bce_losses.push(loss);
        self.train_accuracies.push(train_ac);
        self.test_accuracies.push(test_ac);
    }
    fn write_to_file(&self, fname: impl AsRef<Path>) -> io::Result<()> {
        let mut f = File::create(fname.as_ref())?;
        self.write_as_json(&mut f)
    }
    fn write_as_json<W: Write>(&self, writer: &mut W) -> io::Result<()> {
        write!(writer, "{{")?;
        let mut write_values = |name, values: &[Scalar], is_last: bool| -> io::Result<()> {
            write!(writer, r#""{}": ["#, name)?;
            for (i, v) in values.iter().enumerate() {
                write!(writer, "{}", v)?;
                if i == values.len() - 1 {
                    write!(writer, "]")?;
                } else {
                    write!(writer, ",")?;
                }
            }
            if !is_last {
                write!(writer, ",")
            } else {
                Ok(())
            }
        };
        write_values("loss", &self.bce_losses, false)?;
        write_values("train_ac", &self.train_accuracies, false)?;
        write_values("test_ac", &self.test_accuracies, true)?;
        write!(writer, "}}")
    }
}

struct ArgParse {
    args: HashMap<String, String>,
}

impl ArgParse {
    fn new(args_iter: impl Iterator<Item = String>) -> Self {
        let mut pending_key = None;
        let mut args = HashMap::new();
        for arg in args_iter {
            if let Some(key) = pending_key.take() {
                args.insert(key, arg);
                continue;
            }
            if !arg.starts_with('-') {
                continue;
            }
            let mut idx = 0;
            let len = arg.len();
            while idx < len && arg.as_bytes()[idx] == b'-' {
                idx += 1;
            }
            if let Some(equal) = arg.find('=') {
                if equal == len {
                    panic!("Invalid argument {:?}", arg);
                }
                let key = arg[idx..equal].to_owned();
                let value = arg[equal + 1..].to_owned();
                args.insert(key, value);
            } else {
                pending_key = Some(arg[idx..].to_owned())
            }
        }
        ArgParse { args }
    }
    fn get<V>(&self, query: &str) -> Option<V>
    where
        V: std::str::FromStr,
        V::Err: std::fmt::Debug,
    {
        self.args
            .get(query)
            .map(|v| v.parse().expect("Parse error in ArgParse::get"))
    }
}

#[test]
fn test_arg_parse() {
    let args = ["--flag=1", "--flag2", "0.4", "-flag3", "str"];
    let args = ArgParse::new(args.iter().map(|s| s.to_string()));
    assert_eq!(args.get::<usize>("flag").unwrap(), 1);
    assert_eq!(args.get::<f32>("flag2").unwrap(), 0.4);
    assert_eq!(args.get::<String>("flag3").unwrap(), "str");
    assert!(args.get::<f32>("flag4").is_none());
}
