use crate::classifier::Classifier;
use crate::loader::DataSet;
use crate::optimizer::Optimizer;
use crate::prelude::*;
use crate::random;

/// Train the classifier
pub fn train(
    data: &DataSet,
    minibatch_size: usize,
    aggregate_t: usize,
    deriv_eps: Scalar,
    classifier: &mut Classifier,
    optimizer: &mut dyn Optimizer,
) -> Scalar {
    let mut avg_loss = 0.0;
    let mut counter = 0u32;
    for batch in BatchSampler::new(data, minibatch_size) {
        let loss = batch.train(aggregate_t, deriv_eps, classifier, &mut *optimizer);
        avg_loss += loss;
        counter += 1;
    }
    avg_loss / Scalar::from(counter)
}

/// An iterator to sample batches randomly
struct BatchSampler<'d> {
    data: &'d DataSet,
    minibatch_size: usize,
    batch_indices: Vec<usize>,
    cursor: usize,
}

impl<'d> BatchSampler<'d> {
    fn new(data: &'d DataSet, minibatch_size: usize) -> Self {
        let batch_indices = random::permutation(data.len());
        BatchSampler {
            data,
            minibatch_size,
            batch_indices,
            cursor: 0,
        }
    }
}

impl<'d> Iterator for BatchSampler<'d> {
    type Item = DataSet;
    fn next(&mut self) -> Option<DataSet> {
        let start = self.cursor;
        self.cursor += self.minibatch_size;
        // Drops the small minibatch so that to prevent the gradient from getting noisy
        if self.cursor > self.data.len() {
            None
        } else {
            Some(
                self.data
                    .batch_sample(&self.batch_indices[start..self.cursor]),
            )
        }
    }
}
