//! Common types and utilities
use ndarray::{Array1, Array2, ArrayView1, ArrayView2, ArrayViewMut2};
pub type Scalar = f64;
pub type Vector = Array1<Scalar>;
pub type VectorView<'a> = ArrayView1<'a, Scalar>;
pub type Matrix = Array2<Scalar>;
pub type MatrixView<'a> = ArrayView2<'a, Scalar>;
pub type MatrixViewMut<'a> = ArrayViewMut2<'a, Scalar>;
pub use ndarray::{array, s, Axis, Zip};

pub trait FloatExt {
    fn loge(self) -> Self;
}

impl FloatExt for Scalar {
    fn loge(self) -> Self {
        self.log(1f64.exp())
    }
}
