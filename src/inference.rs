use crate::classifier::Classifier;
use crate::graph::Graph;
use crate::loader::DataSet;
use crate::prelude::*;

/// Returns the accuracy for a dataset
pub fn test_classifier(data: &DataSet, classifier: &Classifier, t: usize) -> Scalar {
    let result = inference(data.graphs(), classifier, t);
    let correct = data
        .labels()
        .iter()
        .zip(result)
        .filter(|(&label, pred)| label == *pred)
        .count();
    let test_size = data.len() as Scalar;
    correct as Scalar / test_size
}

/// Returns the result of prediction
pub fn inference(graph: &[Graph], classifier: &Classifier, t: usize) -> Vec<Scalar> {
    graph
        .iter()
        .map(|g| classifier.predict(g, t).round())
        .collect()
}
