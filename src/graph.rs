use crate::mlp::Mlp;
use crate::prelude::*;

pub fn x0gen_const(d: usize, value: Scalar) -> impl Fn(usize) -> Matrix {
    move |n| Matrix::from_elem((n, d), value)
}

pub fn x0gen_onehot(d: usize, value: Scalar) -> impl Fn(usize) -> Matrix {
    move |n| {
        let mut m = Matrix::zeros((n, d));
        for i in 0..n {
            m[[i, 0]] = value;
        }
        m
    }
}

/// A graph with initial feature set `x0`.
#[derive(Clone, Debug)]
pub struct Graph {
    adj_mat: Matrix,
    x0: Matrix,
}

impl Graph {
    /// Create a graph from an adjacent matrix `adj` and an initial feature matrix `x0`.
    pub fn new(adj: Matrix, x0: Matrix) -> Self {
        assert_eq!(
            adj.dim().0,
            x0.dim().0,
            "Invalid feature shape: {:?}",
            x0.dim()
        );
        Graph { adj_mat: adj, x0 }
    }
    /// Create a graph from an adjacent matrix `adj` and a function `x0gen`, which
    /// takes the number of vartices and returns the initial feature matrix `x0`.
    pub fn from_x0gen(adj: Matrix, x0gen: impl Fn(usize) -> Matrix) -> Self {
        let x0 = x0gen(adj.dim().0);
        Self::new(adj, x0)
    }
    fn aggregate(&self, t: usize, mlp: &Mlp) -> Matrix {
        let mut xt = self.x0.clone();
        for _ in 0..t {
            let at = self.adj_mat.dot(&xt);
            xt = mlp.forward(at.t().view());
        }
        xt
    }
    /// Calcurates the t-times aggregation and READOUT using `mlp`
    pub fn calc_hg(&self, t: usize, m: &Mlp) -> (Vector, Matrix) {
        let xt = self.aggregate(t, m);
        (xt.sum_axis(Axis(0)), xt)
    }

    #[cfg(test)]
    fn calc_hg_linear(&self, t: usize, w: MatrixView) -> (Vector, Matrix) {
        self.calc_hg(t, &Mlp::linear(w.to_owned()))
    }
}

#[test]
fn test_hg() {
    let adj = array![[0., 1., 1.], [1., 0., 1.], [1., 1., 0.],];
    let features = array![[1., 0.], [2., 0.], [3., 0.]];
    let mut graph = Graph::new(adj, features.clone());
    //   x(1,0)  a_1 = (5, 0)
    //  / \
    // y---z(3, 0) a_1 = (3, 0)
    //(2,0) a_1 = (4, 0)
    let weight = array![[1.0, 2.0], [3.0, 4.0]];
    let (hg, xt) = graph.calc_hg_linear(1, weight.view());
    assert_eq!(hg, array![12.0, 36.0]);
    assert_eq!(xt, array![[5., 15.], [4., 12.], [3., 9.]]);
    graph.x0 = xt;
    let hg2 = graph.calc_hg_linear(1, weight.view());
    graph.x0 = features;
    assert_eq!(graph.calc_hg_linear(2, weight.view()), hg2);
    graph.x0 = array![[-1., -1.], [-1., -1.], [-1., -1.]];
    // Check if ReLU works
    assert!(graph
        .calc_hg_linear(2, weight.view())
        .0
        .iter()
        .all(|&x| x == 0.0));
}
