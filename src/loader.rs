use crate::classifier::Classifier;
use crate::graph::Graph;
use crate::optimizer::Optimizer;
use crate::prelude::*;
use std::collections::BTreeMap;
use std::fs;
use std::io::{self, prelude::*, BufReader};
use std::path::{Path, PathBuf};

/// A set of graphs and their labels
#[derive(Debug)]
pub struct DataSet {
    graphs: Vec<Graph>,
    labels: Vec<Scalar>,
}

impl DataSet {
    pub fn labels(&self) -> &[Scalar] {
        &self.labels
    }
    pub fn graphs(&self) -> &[Graph] {
        &self.graphs
    }
    pub fn len(&self) -> usize {
        self.graphs.len()
    }
    /// Split a dataset into two datasets
    pub fn split_off(&mut self, idx: usize) -> Self {
        let graphs = self.graphs.split_off(idx);
        let labels = self.labels.split_off(idx);
        DataSet { graphs, labels }
    }
    /// Samples some datas by `batch_indices` and copies them to a new dataset.
    pub fn batch_sample(&self, batch_indices: &[usize]) -> Self {
        let mut graphs = vec![];
        let mut labels = vec![];
        for &idx in batch_indices {
            graphs.push(self.graphs[idx].clone());
            labels.push(self.labels[idx]);
        }
        DataSet { graphs, labels }
    }
    fn iter(&self) -> impl Iterator<Item = (&Graph, &Scalar)> {
        self.graphs.iter().zip(self.labels.iter())
    }
    pub fn train(
        &self,
        t: usize,
        eps: Scalar,
        classifier: &mut Classifier,
        optimizer: &mut dyn Optimizer,
    ) -> Scalar {
        let minibatch_size = self.len() as Scalar;
        let mut avg_grad = classifier.zeros_like();
        let mut avg_loss = 0.0;
        for (graph, &label) in self.iter() {
            let (grad, loss) = classifier.calc_grad(graph, label, t, eps);
            avg_grad.zip2_for_each(&grad, |avg, current| *avg += current);
            avg_loss += loss;
        }
        avg_grad.for_each(|avg| *avg /= minibatch_size);
        optimizer.update(&avg_grad, classifier);
        avg_loss / minibatch_size
    }
}

fn later<T, U>(tuple: (T, U)) -> U {
    tuple.1
}

pub fn load_train_data(
    datadir: impl AsRef<Path>,
    x0gen: impl Fn(usize) -> Matrix,
) -> io::Result<DataSet> {
    let (mut graphs, mut labels) = (BTreeMap::new(), BTreeMap::new());
    for entry in fs::read_dir(datadir.as_ref().join("train"))? {
        let path = entry?.path();
        match path.file_name().and_then(|s| s.to_str()) {
            Some(s) if s.contains("graph") => {
                let adj = read_adj_mat(fs::File::open(&path)?)?;
                graphs.insert(get_number(s), Graph::from_x0gen(adj, &x0gen));
            }
            Some(s) if s.contains("label") => {
                labels.insert(get_number(s), read_label(fs::File::open(path)?)?);
            }
            _ => panic!("Invalid file {:?} in the training directory", path),
        }
    }
    Ok(DataSet {
        graphs: graphs.into_iter().map(later).collect(),
        labels: labels.into_iter().map(later).collect(),
    })
}

pub fn load_test_data(
    datadir: impl AsRef<Path>,
    x0gen: impl Fn(usize) -> Matrix,
) -> io::Result<Vec<Graph>> {
    let mut graphs = BTreeMap::new();
    for entry in fs::read_dir(datadir.as_ref().join("test"))? {
        let path = entry?.path();
        match path.file_name().and_then(|s| s.to_str()) {
            Some(s) if s.contains("graph") => {
                let adj = read_adj_mat(fs::File::open(&path)?)?;
                graphs.insert(get_number(s), Graph::from_x0gen(adj, &x0gen));
            }
            _ => panic!("Invalid file {:?} in the training directory", path),
        }
    }
    Ok(graphs.into_iter().map(later).collect())
}

fn get_number(s: &str) -> usize {
    s.split('_').next().unwrap().parse().unwrap()
}

fn read_adj_mat<R: Read>(resource: R) -> io::Result<Matrix> {
    let mut reader = BufReader::new(resource);
    let n = next_line(&mut reader)?[0];
    let mut res = Matrix::zeros((n, n));
    for i in 0..n {
        let line = next_line(&mut reader)?;
        for j in 0..n {
            let adj = line[j] as f64;
            res[[i, j]] = adj;
            res[[j, i]] = adj;
        }
    }
    Ok(res)
}

fn read_label<R: Read>(resource: R) -> io::Result<Scalar> {
    let mut reader = BufReader::new(resource);
    let n = next_line(&mut reader)?[0];
    Ok(n as Scalar)
}

fn next_line<R: Read>(r: &mut BufReader<R>) -> io::Result<Vec<usize>> {
    let mut res = String::new();
    r.read_line(&mut res)
        .map(|_| res.split_whitespace().map(|s| s.parse().unwrap()).collect())
}

pub fn default_data_dir() -> PathBuf {
    PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("datasets")
}

#[test]
fn test_read_adj() {
    let path = default_data_dir().join("train/1434_graph.txt");
    let mat = read_adj_mat(fs::File::open(path).unwrap()).unwrap();
    let ans = array![
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
        [0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0],
    ];
    Zip::from(&mat)
        .and(&ans)
        .apply(|loaded, ans| assert_eq!(*loaded, f64::from(*ans)))
}

#[test]
fn test_load() {
    load_train_data(default_data_dir(), |w_dim| {
        crate::random::randn((w_dim, 4), 0.0, 1.0)
    })
    .unwrap();
}
