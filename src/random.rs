use crate::prelude::{FloatExt, Scalar, Vector};
use ndarray::{Array, Dimension, IntoDimension};
use std::cell::RefCell;

/// Create a ndarray with shape `shape` which filled with values sampled from
/// a normal distribution N(`mean`, `stddev`).
pub fn randn<D, Ix>(shape: Ix, mean: Scalar, stddev: Scalar) -> Array<Scalar, D>
where
    D: Dimension,
    Ix: IntoDimension<Dim = D>,
{
    shape_helper(shape, move |n| {
        let mut v = Vector::zeros(n);
        for i in 0..n / 2 {
            let ret = marsaglia_polar(mean, stddev);
            v[i] = ret.0;
            v[i + n / 2] = ret.1;
        }
        if n % 2 != 0 {
            v[n - 1] = marsaglia_polar(mean, stddev).0;
        }
        v
    })
}

fn shape_helper<D, Ix>(shape: Ix, generate_vec: impl Fn(usize) -> Vector) -> Array<Scalar, D>
where
    D: Dimension,
    Ix: IntoDimension<Dim = D>,
{
    let shape = shape.into_dimension();
    let n = shape.size();
    generate_vec(n).into_shape(shape).unwrap()
}

/// Create a permutation of [0..n)
/// https://en.wikipedia.org/wiki/Fisher–Yates_shuffle
pub fn permutation(n: usize) -> Vec<usize> {
    let mut res: Vec<_> = (0..n).collect();
    for i in (1..n).rev() {
        let j = randint() as usize % (i + 1);
        res.swap(i, j);
    }
    res
}

/// Return two values sampled from the Normal distribution N(mean, stddev)
/// https://en.wikipedia.org/wiki/Marsaglia_polar_method
fn marsaglia_polar(mean: Scalar, stddev: Scalar) -> (Scalar, Scalar) {
    let (mut s, mut u, mut v) = (0.0, 0.0, 0.0);
    while s >= 1.0 || s == 0.0 {
        u = randfloat() * 2.0 - 1.0;
        v = randfloat() * 2.0 - 1.0;
        s = u * u + v * v;
    }
    let mul = (-2.0 * s.loge() / s).sqrt();
    let res1 = u * mul * stddev + mean;
    let res2 = v * mul * stddev + mean;
    (res1, res2)
}

thread_local! {
    static THREAD_RNG: RefCell<XorShift> = RefCell::new(XorShift::new());
}

/// Generates a random float value from [0, 1)
pub fn randfloat() -> Scalar {
    let rand = f64::from(randint());
    rand / f64::from(u32::max_value())
}

/// Generates a random integer from [0, u32::max_value( + 1)
pub fn randint() -> u32 {
    THREAD_RNG.with(|rng| rng.borrow_mut().next())
}

/// Xorshift128 random number generator
/// https://en.wikipedia.org/wiki/Xorshift
pub struct XorShift {
    x: u32,
    y: u32,
    z: u32,
    w: u32,
}

impl XorShift {
    fn new() -> Self {
        XorShift {
            x: 123456789,
            y: 362436069,
            z: 521288629,
            w: 88675123,
        }
    }
    fn next(&mut self) -> u32 {
        let x = self.x;
        let t = x ^ (x << 11);
        self.x = self.y;
        self.y = self.z;
        self.z = self.w;
        let w = self.w;
        self.w = w ^ (w >> 19) ^ (t ^ (t >> 8));
        self.w
    }
}

#[test]
fn test_randint() {
    let mut set = std::collections::HashSet::new();
    for _ in 0..100 {
        let rn = randint();
        set.insert(rn);
    }
    assert_eq!(set.len(), 100)
}

#[test]
fn test_randn() {
    let v = randn(1000, 0.0, 1.0);
    let s = v.sum();
    assert!(-10. < s && s < 10.);
}

#[test]
fn test_permutation() {
    use std::collections::HashSet;
    let p = permutation(100);
    assert_eq!(p.into_iter().collect::<HashSet<_>>().len(), 100);
}
