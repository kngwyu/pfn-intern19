import argparse
import json
from matplotlib.lines import Line2D
from matplotlib import pyplot as plt
from typing import List


COLORS = ['midnightblue', 'lightseagreen', 'greenyellow',
          'lightcoral', 'dimgray', 'darkorange', 'fuchsia']
STYLES = [(0, (5, 5)), (0, (1, 5)), (0, (1, 1)), (0, (3, 5, 1, 5)),
          (0, (3, 1, 1, 1)), (0, (3, 1, 1, 1, 1, 1)), (0, ())]


def args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', metavar='F', type=str, nargs='+')
    parser.add_argument('--value', type=str, default='test_ac')
    parser.add_argument('--title', type=str, default='')
    return parser.parse_args()


def load_file(fname: str) -> dict:
    def mul100(li: List[float]) -> None:
        li = [x * 100.0 for x in li]
    with open(fname) as f:
        result = json.load(f)
        mul100(result['train_ac'])
        mul100(result['test_ac'])
    return result


def remove_ext(s: str) -> str:
    pos = s.find('.')
    return s[:pos] if pos > 0 else s


def plot(results: dict, key: str) -> List[Line2D]:
    res = []
    for i, r in enumerate(results):
        p = plt.plot(r[key])[0]
        p.set_color(COLORS[i])
        p.set_linestyle(STYLES[i])
        res.append(p)
    return res


def main(args: argparse.Namespace) -> None:
    VAL_TO_YLABEL = {
        'loss': 'Loss',
        'train_ac': 'Accuracy in the training data(percentage)',
        'test_ac': 'Accuracy in the test data(percentage)',
    }
    results = [load_file(f) for f in args.filename]
    lines = plot(results, args.value)
    plt.xlabel('Optimization epochs')
    plt.ylabel(VAL_TO_YLABEL[args.value])
    plt.title(args.title)
    legends = [remove_ext(f) for f in args.filename]
    plt.legend(lines, legends, bbox_to_anchor=(1, 0), loc='lower right')
    plt.show()


if __name__ == "__main__":
    main(args())
